/*
 *! @file verification.h
 */

#ifndef __VERIFICATION_DEFINED
#define __VERIFICATION_DEFINED

#include <stdio.h>

/* global matrix to represent sudoku puzzle */
extern int sudokuMatrix[9][9];

/** Checks if there are repeated elements in row.
 *  @param row identifies the row in the matrix.
 *  @return the first repeated element if it exists or zero otherwise.
 */
int verifyRow (int row);

/** Checks if there are repeated elements in column.
 * @param column identifies the column in the matrix.
 * @return the first repeated element if it exists or zero otherwise.
 */
int verifyColumn(int column);

/** Checks if there are repeated elements in block
 *   (numbered from 0 to 8. Each block has nine elements).
 *  @param block identifies the block in the matrix.
 *  @return the first repeated element if it exists or zero otherwise.
 */
int verifyBlock(int block);

#endif
