/*
 *! @file thread.c
 */

#include "thread.h"

void* threadRow (void* rowArgument)
{
  VerificationRange *verRange = (VerificationRange*)rowArgument;
  int i;
  for(i = verRange->first; i < (verRange->first + verRange->count); i++)
    {
      int tmpResult = verifyRow(i);
      if(tmpResult != 0)
        {
          VerificationResult* verResult = (VerificationResult*)malloc(sizeof(VerificationResult));
          verResult->id = i+1;
          verResult->repeatedNumber = tmpResult;
          return verResult;
        }

    }
  VerificationResult* verResult = (VerificationResult*)malloc(sizeof(VerificationResult));
  verResult->id = 0;
  verResult->repeatedNumber = 0;
  return verResult;
} /* threadRow */

void* threadColumn (void* columnArgument)
{
  VerificationRange *verRange = (VerificationRange*)columnArgument;
  int i;
  for(i = verRange->first; i < (verRange->first + verRange->count); i++)
    {
      int tmpResult = verifyColumn(i);
      if(tmpResult != 0)
        {
          VerificationResult* verResult = (VerificationResult*)malloc(sizeof(VerificationResult));
          verResult->id = i+1;
          verResult->repeatedNumber = tmpResult;
          return verResult;
        }

    }
  VerificationResult* verResult = (VerificationResult*)malloc(sizeof(VerificationResult));
  verResult->id = 0;
  verResult->repeatedNumber = 0;
  return verResult;

} /* threadColumn */

void* threadBlock (void* blockArgument)
{
  VerificationRange *verRange = (VerificationRange*)blockArgument;
  int i;
  for(i = verRange->first; i < (verRange->first + verRange->count); i++)
    {
      int tmpResult = verifyBlock(i);
      if(tmpResult != 0)
        {
          VerificationResult* verResult = (VerificationResult*)malloc(sizeof(VerificationResult));
          verResult->id = i+1;
          verResult->repeatedNumber = tmpResult;
          return verResult;
        }

    }
  VerificationResult* verResult = (VerificationResult*)malloc(sizeof(VerificationResult));
  verResult->id = 0;
  verResult->repeatedNumber = 0;
  return verResult;

} /* threadBlock */
