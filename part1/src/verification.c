/*
 *! @file verification.c
 */

#include "verification.h"

int verifyRow (int row)
{
  int i;
  int set = 0;
  int ans = 0;
  int elementMatrix;
  for (i = 0; i < 9; i++)
    {
      elementMatrix = sudokuMatrix[row][i];
      if(set & (1 << elementMatrix)) {
	ans = elementMatrix;
	break;
      }
      else {
	set |= 1 << elementMatrix;
      }
    }
  return ans;
} /* verifyRow */


int verifyColumn(int column)
{
  int i;
  int set = 0;
  int ans = 0;
  int elementMatrix;
  for (i = 0; i < 9; i++)
    {
      elementMatrix = sudokuMatrix[i][column];
      if(set & (1 << elementMatrix)) {
	ans = elementMatrix;
	break;
      }
      else {
	set |= 1 << elementMatrix;
      }
    }
  return ans;
} /* verifyColumn */

int verifyBlock(int block)
{
  int i, j;
  int elementMatrix;
  int set = 0;
  int ans = 0;
  int row = (block / 3) * 3;
  int column = (block % 3) * 3;
  for(i = row; i < row+3; i++)
    for(j = column; j < column+3; j++)
      {
	elementMatrix = sudokuMatrix[i][j];
	if(set & (1 << elementMatrix)) {
	  ans = elementMatrix;
	  break;
	}
	else {
	  set |= 1 << elementMatrix;
	}
      }
  return ans;
} /* verifyBlock */
