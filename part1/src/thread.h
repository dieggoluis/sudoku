/*
 *! @file thread.h
 */

#ifndef __THREAD_DEFINED
#define __THREAD_DEFINED

#include <stdio.h>
#include <stdlib.h>
#include "verification.h"

//struct used to save result of threads
typedef struct VerificationResult
{
  int id;
  int repeatedNumber;
} VerificationResult;

//struct used for limiting the range of each thread
typedef struct VerificationRange
{
  int first;
  int count;
} VerificationRange;

/** Checks if there are repeated elements in each one of the lines in a specified group of lines.
 *  @param rowArgument is a struct containing the number of the first line and how many lines will be verified
 *  @return a struct that has the line of the first repeated element and the repeated element itself if it exists or zeros otherwise.
 */
void* threadRow (void* rowArgument);

/** Checks if there are repeated elements in each one of the columns in a specified group of columns.
 *  @param columnArgument is a struct containing the number of the first column and how many columns will be verified
 *  @return a struct that has the column of the first repeated element and the repeated element itself if it exists or zeros otherwise.
 */
void* threadColumn(void* columnArgument);

/** Checks if there are repeated elements in each one of the blocks in a specified group of blocks.
 *  @param rowArgument is a struct containing the number of the first block and how many blocks will be verified
 *  @return a struct that has the number of the block of the first repeated element and the repeated element itself if it exists or zeros otherwise.
 */
void* threadBlock(void* blockArgument);

#endif
