/*
 *! @file main.c
 */

#include <pthread.h>
#include "thread.h"

#define MAX_THREADS 27

int sudokuMatrix[9][9];		               /* global matrix to represent sudoku puzzle */
pthread_t threads[MAX_THREADS];	               /* vector of threads */
VerificationRange threadsParam[MAX_THREADS];   /* parameters of each thread */

/** Distributes threads
 *  @param f is the function to be passed to the threads
 *  @param Nthreads is the number of people
 *  @param threadOffset represent the offset to creation of each thread
 *  @return void
 */
void distributeLoad(void * (*f)(void*), int Nthreads, int threadOffset)
{
  int i;
  int loadPerThread = 9/Nthreads;
  int threadId;
  //all threads, except for the last one, are responsible for floor(9/Nthreads) parts
  for(i = 0; i < Nthreads-1; i++)
    {
      threadId = threadOffset + i;
      threadsParam[threadId].first = loadPerThread * i;
      threadsParam[threadId].count = loadPerThread;
      pthread_create(&threads[threadId], NULL, f, &threadsParam[i]);
    }
  
  //the last thread can possibly do more work
  threadId = threadOffset + Nthreads - 1;
  threadsParam[threadId].first = loadPerThread * i;
  threadsParam[threadId].count = 9 - loadPerThread * i;
  pthread_create(&threads[threadId], NULL, f, &threadsParam[i]); 
} /* distributeLoad */


/* Main function */
int main (int argc, char**argv)
{
  int Nthreads,i,j;
  if(argc == 1)
    {
      //help
      printf("Este programa tem como entrada uma instância de sudoku(9x9) e verifica se ela é válida.\nComo parametro é possível indicar o número de threads ( multiplo de 3), que serão utilizados Nthreads, 3 <= Ntrheads <= 27.\n");
	return 0;
    }

  //reads number of threads
  sscanf(argv[1],"%d",&Nthreads);
  if(Nthreads%3 != 0 || Nthreads <= 0 || Nthreads > 27)
    {
      printf("Número de threads inválido.");
      return 0;
    }
  
  Nthreads/=3;
  //reads the sudoku matrix
  for(i = 0;i<9;i++)
    {
      for(j = 0; j < 9;j++)
	scanf("%d",&sudokuMatrix[i][j]);
    }
  
  //distributes loads among the threads
  distributeLoad(threadRow,Nthreads,0);
  distributeLoad(threadColumn,Nthreads,Nthreads);
  distributeLoad(threadBlock,Nthreads,2*Nthreads);
  
  //join the threads and reconstruct the results
  VerificationResult * result;
  int valid = 1;
  for (i = 0; i < 3*Nthreads; i++)
    {
      pthread_join(threads[i], (void**)&result);
      if (result->id != 0)
	{
	  if (i < Nthreads)
	    {
	      printf("A linha ");
	    }
	  else if (i < 2 * Nthreads)
	    {
	      printf("A coluna ");
	    }
	  else
	    {
	      printf("O bloco ");
	    }
	  printf ("%d contém duas ocorrência do número %d.\n", result->id, result->repeatedNumber);
	  valid = 0;
	  break;
	}
    }

  if(valid)
    {
      printf("O diagrama é válido.\n");
    }
  
  return 0;
} /* main */


