#Verificação

Este módulo verifica se um diagrama Sudoku completo 9x9 foi preenchido corretamente 

##Requerimentos
***************
  - Compilador GCC com a biblioteca Pthread
  - Sistema Operacional UNIX

##Entrada
*********
O programa recebe como entrada uma matriz 9x9 que representa um diagrama sudoku. Abaixo segue um exemplo de entrada:

          8 6 1 3 4 7 2 9 5
          4 3 2 8 9 5 1 6 7
          9 5 9 1 6 2 4 8 3
          2 7 8 4 5 1 6 3 9 
          5 4 9 6 8 3 7 2 1 
          6 1 3 2 7 9 8 5 4
          3 2 4 9 1 8 5 7 6
          1 8 5 7 3 6 9 4 2
          7 9 6 5 2 4 3 1 8

##Saída
*******
Se o programa detectar algum problema no diagrama que lhe foi passado ele informará onde ocorreu o erro e qual elemento é responsável pelo erro. Abaixo segue um exemplo de saída onde o diagrama sudoku fornecido na entrada possuia um erro:

           A linha 3 contém duas ocorrências do número 9.

Caso o diagrama sudoku fornecido esteja correto, a saída será:

           O diagrama é válido.

##Compilação
************

A compilação do módulo é feita executando o seguinte comando:

           make

##Execução
**********

A execução do módulo é feita executando o seguinte comando:

           ./main Ntrheads </caminho/para/o/arquivo/de/entrada/arquivo

No exemplo acima Ntrheads é o número de threads que serão usadas para resolver o problema, onde 3 <= Ntrheads <= 27 e Ntrheads é um multiplo de 3.
