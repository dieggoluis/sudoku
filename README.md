#GRUPO:	
	Nome                       RA
	Allan Sapucaia Barboza     134796
	Diego Luis de Souza        135491
	Luis Fernando Antonioli	   136710
	 
	
Esse arquivo faz uma breve análise das nossas escolhas para a resolução dos problemas propostos utilizando vários threads.

Primeiramente, optamos por tornar o número de threads uma escolha do usuário, impondo restrições quando as mesmas facilitam consideravelmente nosso trabalho. Dessa forma, é possível avaliar o desempenho em função do número de threads e encontrar o melhor número de threads para cada computador e problema. Essa escolha é feita como parâmetro do programa.



##Primeira parte: Verificação
*****************************
Optamos por dividir os threads em 3 grupos: colunas, linhas e blocos. Os threads do mesmo grupo dividem as linhas/colunas/blocos entre si, sobrando mais trabalho para o último quando a divisão não é exata. Por isso, o usuário deve entrar com um número de threads múltiplo de 3, já que cada grupo terá Nthreads/3 threads.
A distribuição é simples e explora o fato de que as linhas/colunas e blocos são independentes. Uma desvantagem é a impossibidade de não utilizar uma quantidade não-multipla de 3 de threads.



##Segunda parte: Dicas
**********************
Mantivemos uma distribuição de trabalho muito parecida com a anterior. As threads são divididas da mesma maneira e calculam o conjunto (bitmask) de opções válidas para aquela linha/coluna/bloco. Dessa forma, as opções para um determinado elemento são a interseção entre esses 3 conjuntos calculados para suas coordenadas (o que reduz a complexidade do problema para O(n²)). Esse cálculo final é feito na hora da impressão, no main.




##Terceira parte: Resolução
***************************
Assumimos que as entradas estarão corretas (ao contrário do exemplo do site).

Tentamos explorar uma ideia um pouco diferente para resolver esse problema. Modelamos o espaço de busca como um grafo com estrutura de árvore e exploramos as buscas em largura e profundidade (backtracking).

Como queríamos o número de thread variável, tentamos elaborar uma maneira para distribuir os threads entre os diversos ramos da árvore. As ideias inicias consistiam em distribuir os threads até atingir um nível de profundidade, ou atribuir aos Nthreads primeiros nós da árvore, mas refletimos sobre o seguinte fato: Dados três vértices, w, u e v,  sendo w antecessor de u e v, atribuir um thread para cada um deles é redundante, poderíamos fazer isso só para u e v, já que o w tem que esperar da mesma forma.

	        w
	      /   \
	    u       v

Assim, decidimos que qualquer ramo do estado inicial (raíz) às folhas (soluções ou becos-sem-saída) deveria ter um único thread.
Para implementar a ideia começamos com uma busca em largura, que vai construindo um fila e descendo por camadas nessa árvore, se em um determinado momento o número de elementos na fila é maior ou igual ao número de threads sugerido pelo usuário, cada um desses vértices (que correspondem a estados) é transformado em uma thread que rodará uma DFS (backtracking) a partir desse estado. Esses nós correspondem a um corte na árvore.

Apesar dessa escolha não ser ótima (conhecendo toda a árvore seria possível definir um corte ótimo, mas o problema já estaria resolvido….) parece bastante razoável e tem bons resultados práticos.

Essa aproximação é heurística e pode ocorrer de todo o problema ser resolvido por BFS, caso a fila nunca fique tão grande quando o usuário deseja. Por outro lado, se o número de threads for da ordem do número de núcleos dos computadores pessoais (entre 1 e 10), os threads serão criados em qualquer instância interessante do problema.

A vantagem dessa abordagem é explorar a paralelização sem desperdiçar threads. Por outro lado ela é complicado e os casos degenerados (apenas uma BFS) são mais lentos do que uma simples busca.
