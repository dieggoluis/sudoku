/*
 *! @file hint.h
 */

#ifndef __HINT_DEFINED
#define __HINT_DEFINED

#include <stdio.h>

/* global matrix to represent the sudoku puzzle */
extern int sudokuMatrix[9][9];

/** Finds the avaliable numbers in the row.
 *  @param row identifies the row in the matrix.
 *  @return the set of the avaliable numbers.
 */
int availableNumbersForRow (int row);

/** Finds the avaliable numbers in the column.
 * @param column identifies the column in the matrix.
 * @return the set of the avaliable numbers.
 */
int availableNumbersForColumn(int column);

/** Finds the avaliable numbers in the block.
 *   (numbered from 0 to 8. Each block has nine elements).
 *  @param block identifies the block in the matrix.
 *  @return the set of the avaliable numbers.
 */
int availableNumbersForBlock(int block);

#endif
