/*
 *! @file main.c
 */

#include <ctype.h>
#include <pthread.h>
#include "hint.h"
#include "thread.h"

#define MAX_THREADS 27

int sudokuMatrix[9][9]; /* global matrix to represent sudoku puzzle */
int avalRows[9];	/* allowed numbers on each row */
int avalCols[9];	/* allowed numbers on each column */
int avalBlocks[9];	/* allowed numbers on each block */

pthread_t threads[MAX_THREADS];	              /* vector of threads */
VerificationRange threadsParam[MAX_THREADS];  /* parameters of each thread */

/** Distributes threads
 *  @param f is the function to be passed to the threads
 *  @param Nthreads is the number of people
 *  @param threadOffset represent the offset to creation of each thread
 *  @return void
 */
void distributeLoad (void * (*f)(void*), int Nthreads, int threadOffset)
{
  int i;
  int loadPerThread = 9/Nthreads;
  int threadId;
  //all threads, except for the last one, are responsible for floor(9/Nthreads) parts
  for(i = 0; i < Nthreads-1;i++)
    {
      threadId = threadOffset + i;
      threadsParam[threadId].first = loadPerThread*i;
      threadsParam[threadId].count = loadPerThread;
      pthread_create(&threads[threadId],NULL,f,&threadsParam[i]);
    }
  
  //the last thread can possibly do more work
  threadId = threadOffset + Nthreads-1;
  threadsParam[threadId].first = loadPerThread*i;
  threadsParam[threadId].count = 9-loadPerThread*i;
  pthread_create(&threads[threadId],NULL,f,&threadsParam[i]); 
} /* distributeLoad */


/** Finds the correspondent block
 *  @param i identifies the row in the matrix.
 *  @param j identifies the column in the matrix.
 *  @return the correspondent block.
 */
inline int whichBlock (int i, int j)
{
  return (int)(i/3) * 3 + (int)(j/3);
}

/* Main funtion */
int main (int argc, char**argv)
{
  int i, j, t;
  char c;
  int Nthreads;
  if(argc == 1)
    {
      //help
      printf("Este programa tem como entrada uma instância incompleta de sudoku(9x9) e retorna dicas para seu preenchimento.\nComo parametro é possível indicar o número de threads ( multiplo de 3), que serão utilizados Nthreads, 3 <= Ntrheads <= 27.\n");
      return 0;
    }

  //reads number of threads
  sscanf(argv[1], "%d", &Nthreads);
  if(Nthreads%3 != 0 || Nthreads <= 0 || Nthreads > 27)
    {
      printf("Número de threads inválido.");
      return 0;
    }
  
  Nthreads/=3;

  //reads the sudoku matrix
  for (i = 0; i < 9; i++)
    {
      for(j = 0; j < 9; j++)
	{
	  char c; 
	  scanf(" %c", &c);
	  if(c == 'X')
	    sudokuMatrix[i][j] = -1;
	  else
	    sudokuMatrix[i][j] = c-'0';
	}
    }

  distributeLoad(threadRows,Nthreads,0);
  distributeLoad(threadColumns,Nthreads,Nthreads);
  distributeLoad(threadBlocks,Nthreads,2*Nthreads);

  for(i = 0;i < 3*Nthreads;i++)
    pthread_join(threads[i],NULL);

  // prints matrix with hints
  for (i = 0; i < 9; i++)
    {
      for (j = 0; j < 9; j++)
	{
	  if(sudokuMatrix[i][j] != -1)
	    printf("%d ", sudokuMatrix[i][j]);
	  else
	    {
	      printf("(");
	      int tmp = avalRows[i] & avalCols[j];
	      tmp &= avalBlocks[whichBlock(i, j)];
	      for(t = 1; t <= 9; t++){
		if(tmp & (1 << t)) printf("%d", t);
	      }
	      printf(") ");
	    }
	}
      printf("\n");
    }
  
  return 0;
} /* main */


