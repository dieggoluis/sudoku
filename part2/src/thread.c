/*
 *! @file thread.c
 */

#include "thread.h"

void * threadRows (void * arg)
{
  VerificationRange range = *(VerificationRange*)arg;
  int i;
  for(i = 0; i < range.count;i++)
    avalRows[i+range.first] = availableNumbersForRow(i+range.first);
  return NULL;
}

void * threadColumns (void * arg)
{
  VerificationRange range = *(VerificationRange*)arg;
  int i;
  for(i = 0; i < range.count;i++)
    avalCols[i+range.first] = availableNumbersForColumn(i+range.first);
  return NULL;
}

void * threadBlocks (void * arg)
{
  VerificationRange range = *(VerificationRange*)arg;
  int i;
  for(i = 0; i < range.count;i++)
    avalBlocks[i+range.first] = availableNumbersForBlock(i+range.first);
  return NULL;
}

