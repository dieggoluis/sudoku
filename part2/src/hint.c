/*
 *! @file hint.c
 */

#include "hint.h"

int availableNumbersForRow(int row)
{
  int i;
  int set = -1;
  int elementMatrix;
  for (i=0;i<9;i++)
  {
    elementMatrix = sudokuMatrix[row][i];
    if(elementMatrix != -1)
      set = set & (~(1 << elementMatrix));
  }
  return set;
} /* availableNumbersForRow */


int availableNumbersForColumn(int column)
{
  int i;
  int set = -1;
  int elementMatrix;
  for (i=0;i<9;i++)
    {
      elementMatrix = sudokuMatrix[i][column];
      if(elementMatrix != -1)
	set= set &(~(1 << elementMatrix));
    }
  return set;
} /* availableNumbersForColumn */


int availableNumbersForBlock(int block)
{
  int i, j;
  int elementMatrix;
  int set = -1;
  int row = (block / 3) * 3;
  int column = (block % 3) * 3;

  for(i = row; i < row + 3; i++)
    {
      for(j = column; j < column + 3; j++)
	{
	  elementMatrix = sudokuMatrix[i][j];
	  if(elementMatrix != -1)
	    set= set &(~(1 << elementMatrix));
	}
    }
  return set;
} /* availableNumbersForBlock */
