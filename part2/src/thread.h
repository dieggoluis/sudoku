/*
 *! @file thread.h
 */

#ifndef __THREAD_DEFINE
#define __THREAD_DEFINE

#include "hint.h"

//struct used for limiting the range of each thread
typedef struct VerificationRange
{
  int first;
  int count;
} VerificationRange;

//imports the integers representing the sets of available numbers
extern int avalRows[9], avalCols[9], avalBlocks[9];


/** Implements the thread responsible por the range "arg" of rows
 *  @param arg identifies the range
 *  @return void
 */
void * threadRows(void * arg);

/** Implements the thread responsible por the range "arg" of columns
 *  @param arg identifies the range
 *  @return void
 */
void * threadColumns(void * arg);

/** Implements the thread responsible por the range "arg" of blocks
 *  @param arg identifies the range
 *  @return void
 */
void * threadBlocks(void * arg);

#endif
