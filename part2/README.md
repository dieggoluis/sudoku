#Dicas

Este módulo ajuda o usuário sugestionando dicas de como resolver um diagrama Sudoku 9x9.

##Requerimentos
***************
  - Compilador GCC com a biblioteca Pthread
  - Sistema Operacional UNIX

##Entrada
*********
O programa recebe como entrada uma matriz 9x9 que representa um diagrama sudoku. Neste diagrama, X representa um elemento que ainda não foi definido . Abaixo segue um exemplo de entrada:

          X 6 1 X X X X 9 1
          4 3 X X 9 5 X 6 X
          9 X X 1 X 2 X X 3
          X X X 4 X 1 X X 9
          5 X 9 X X X 7 X 1
          6 X X 2 X 9 X X X
          3 X X 9 X 8 X X 6
          X 8 X 7 3 X X 4 2
          X 9 X X X X 3 1 X


##Saída
*******
O programa analisará os possíveis valores dos elementos que ainda não foram definidos e irá mostrar todas as possibilidades.
Para melhor visualização os números sugeridos estarão entre parênteses indicando quais valores podem ser colocados em cada posição. Abaixo segue um exemplo de saída:

          (278)      6      1      (38)      (478)      (347)      (2458)      9      1 
          4      3      (278)      (8)      9      5      (28)      6      (78) 
          9      (57)      (578)      1      (4678)      2      (458)      (578)      3 
          (278)      (27)      (2378)      4      (5678)      1      (2568)      (2358)      9 
          5      (24)      9      (368)      (68)      (36)      7      (238)      1 
          6      (147)      (3478)      2      (578)      9      (458)      (358)      (458) 
          3      (12457)      (2457)      9      (1245)      8      (5)      (57)      6 
          (1)      8      (56)      7      3      (6)      (59)      4      2 
          (27)      9      (24567)      (56)      (2456)      (46)      3      1      (578) 


##Compilação
************

A compilação do módulo é feita executando o seguinte comando:

           make

##Execução
**********

A execução do módulo é feita executando o seguinte comando:

           ./main Ntrheads </caminho/para/o/arquivo/de/entrada/arquivo

No exemplo acima Ntrheads é o número de threads que serão usadas para resolver o problema, onde 3 <= Ntrheads <= 27 e Ntrheads é um multiplo de 3.
