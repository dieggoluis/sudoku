/*
 *! @file thread.c
 */

#include "thread.h"
#include "solve.h"

void threadCreate (State state)
{
  State *s = (State*)malloc(sizeof(State));
  *s = state;
  pthread_create (&threads[countThreads++], NULL, backtrackThread, (void*)s);
} /* threadCreate */


void * backtrackThread (void * arg)
{ 
  State *s = (State *)malloc(sizeof(State));
  *s = *(State*)arg;

  Result *r = malloc(sizeof(Result));
  *r = solveDFS(s);
  return r;
} /* backtrackThread */


Result waitThread ()
{
  int i;
  Result *r;
  for (i = 0; i < countThreads; i++ )
    {      
      pthread_join(threads[i], (void**)&r);
      if(r->solved) 
	{
	  return *r;
	}
    }    
  
  Result fail;
  fail.solved = 0;
  return fail;
} /* waitThread */



