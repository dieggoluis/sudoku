/*
 *! @file thread.h
 */

#ifndef _THREAD_DEFINE
#define _THREAD_DEFINE

#include <stdio.h>
#include <pthread.h>

#define MAX_THREADS 109

pthread_t threads[MAX_THREADS]; /* vector of threads */
int countThreads;		/* count number of threads */

/* struct to store thread parameters */
typedef struct State{
  int sudokuMatrix[9][9];
  int availableNumbersRow[9], availableNumbersColumn[9], availableNumbersBlock[9];
  int r, c;
} State;

/* struct to store thread result */
typedef struct Result{
  int solved;
  int sudokuMatrix[9][9];
} Result;

/** creates one thread with suitable parameters
 *  @param state indentifies the backtracking state
 *  @return void
 */
void threadCreate(State state);

/** copy current state and call solve by breadth-first search
 *  @param arg identifies current state
 *  @return void
 */
void * backtrackThread(void * arg);

/** waiting threads finish and return if sudoku was solved or not
 *  @return the result (solved or not with current matrix)
 */
Result waitThread();

#endif
