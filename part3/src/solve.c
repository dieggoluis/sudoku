/*
 *! @file solve.c
 */

#include "thread.h"
#include "solve.h"

extern int sudokumatrix[9][9];
void createQueue(StateQueue * queue,int capacity)
{
  queue->data = malloc(sizeof(State)*capacity);
  queue->start=queue->end=0;
  queue->size = 0;
  queue->capacity = capacity;
}

void deleteQueue(StateQueue * queue)
{
  free(queue->data);
}

void insertQueue(StateQueue * queue,State next)
{
  queue->data[queue->end] = next;
  queue->end = (queue->end+1)%queue->capacity;
  queue->size++;
}

State removeQueue(StateQueue * queue)
{
  State next = queue->data[queue->start];
  queue->start = (queue->start+1)%queue->capacity;
  queue->size--;
  return next;
}

int getBlockNumber(int r,int c)
{
  return (int)(r/3) * 3 + (int)(c/3);
}

int getAvailableNumbers(const State * s)
{
  int block = getBlockNumber(s->r,s->c);
  return s->availableNumbersRow[s->r] & s->availableNumbersColumn[s->c] & s->availableNumbersBlock[block];
}

void printState (State * s)
{
  int i,j;
  printf("\n");
  for(i=0;i<9;i++)
    {
      for(j=0;j<9;j++)
	{
	  printf("%d ",s->sudokuMatrix[i][j]);
	}
      printf("\n");
    }
  printf("(%d %d)\n",s->r,s->c);
}

State buildNextState(State * cur,int num)
{
  State next;
  int i,j;
  int block;
  
  int mask = 1 << num;
  //copies the information
  for(i = 0; i < 9;i++)
    {
      next.availableNumbersRow[i] = cur->availableNumbersRow[i];
      next.availableNumbersColumn[i] = cur->availableNumbersColumn[i];
      next.availableNumbersBlock[i] = cur->availableNumbersBlock[i];
    }

  for(i = 0;i < 9;i++)
    for(j = 0;j< 9;j++)
      next.sudokuMatrix[i][j] = cur->sudokuMatrix[i][j];
  
  //updats the insetion
  next.sudokuMatrix[cur->r][cur->c] = num;

  block = getBlockNumber(cur->r,cur->c);

  next.availableNumbersRow[cur->r] &= ~mask;
  next.availableNumbersColumn[cur->c] &= ~mask;
  next.availableNumbersBlock[block] &= ~mask;

  //calculates the next position
  if(cur->r == 8)
    {
      next.r = 0;
      next.c = cur->c+1;
    }
  else
    {
      next.r = cur->r+1;
      next.c = cur->c;
    }
  return next;
} /* buildNextState */

Result solveBFS(State start,int Nthreads)
{
  StateQueue queue; 
  Result result;
  int i,j;

  State cur,next;
  int availableNumbers,curElement,num;
  createQueue(&queue,Nthreads+9);
  insertQueue(&queue,start);

  result.solved = 0;
  while(queue.size > 0)
    {
      cur = removeQueue(&queue);
      //verifies if the puzzle is already solved ( all the columns were filled)
      if(cur.c >= 9)
	{	  
	  for(i = 0;i < 9;i++)
	    for(j = 0;j< 9;j++)
	      result.sudokuMatrix[i][j] = cur.sudokuMatrix[i][j];

	  result.solved = 1;
	  break;
	}
      
      curElement = cur.sudokuMatrix[cur.r][cur.c];
      //the element is empty
      if(curElement == -1)
	{
	  availableNumbers = getAvailableNumbers(&cur);
	  //tries every available number
	  for(i = 1; i <= 9;i++)
	    {
	      num = 1 << i;
	      if( (num & availableNumbers) != 0)
		{
		  next = buildNextState(&cur,i);
		  insertQueue(&queue,next);
		}
	  
	    }
	}
      else
	{
	  next = buildNextState(&cur,curElement);
	  insertQueue(&queue,next);
	}
      
      //verifies if the queue is larger than the number of suggested threads
      if(queue.size >= Nthreads)
	{
	  //creates a new thread, to run a DFS, for each state in the queue
	  for(i = queue.start; i != queue.end; i = (i+1)%(queue.capacity))
	    {
	      threadCreate(queue.data[i]);
	    }
	  //waits for the results
	  result = waitThread();
	  break;
	}
      
    }
  deleteQueue(&queue);
  return result;
} /* solveBFS */

Result solveDFS(State * start)
{
  Result result;
  int i,j;
  int availableNumbers;
  int mask;
  int r,c,block;
  //verifies if the puzzle is already solved
  if(start->c > 8)
    {
      result.solved = 1;
      for(i=0;i<9;i++)
	for(j=0;j<9;j++)
	  result.sudokuMatrix[i][j] = start->sudokuMatrix[i][j];
      return result;
    }
  result.solved = 0;
  //the position is filled, skip
  if(start->sudokuMatrix[start->r][start->c] != -1)
    {
      start->r++;
      if(start->r > 8)
	{
	  start->r = 0;
	  start->c++;
	}
      return solveDFS(start);
    }
  else
    {
      availableNumbers = getAvailableNumbers(start);
      r = start->r;
      c = start->c;
      block = getBlockNumber(r,c);

      start->r++;
      if(start->r > 8)
	{
	  start->r = 0;
	  start->c++;
	}
	      
      for(i = 1; i <= 9;i++)
	{
	  mask = 1 << i;
	  if( (mask & availableNumbers) != 0)
	    {
	      //removes the number from the available set
	      start->sudokuMatrix[r][c] = i;
	      
	      start->availableNumbersRow[r] &= ~mask;
	      start->availableNumbersColumn[c] &= ~mask;
	      start->availableNumbersBlock[block] &= ~mask;

	      result = solveDFS(start);
	      if(result.solved == 1)
		return result;
	      //returns the number too the available set
	      start->availableNumbersRow[r] |= mask;
	      start->availableNumbersColumn[c] |= mask;
	      start->availableNumbersBlock[block] |= mask;
	    }
	}
    }
  //restore the state before returning
  start->r = r;
  start->c = c;
  start->sudokuMatrix[r][c] = -1;
  return result;
} /* solveDFS */

Result solve(int Nthreads)
{
  int i,j;
  Result result;
  State start;
  for(i = 0; i < 9;i++)
    for(j = 0;j < 9;j++)
      start.sudokuMatrix[i][j] = sudokuMatrix[i][j];
  start.r = start.c = 0;

  //use the part 2 (hints) to gather the availability information
  for(i = 0;i < 9;i++)
    {
      start.availableNumbersRow[i] = availableNumbersForRow(i);
      start.availableNumbersColumn[i] = availableNumbersForColumn(i);
      start.availableNumbersBlock[i] = availableNumbersForBlock(i);
    }
  result = solveBFS(start,Nthreads);
  return result;
}
