/*
 *! @file solve.h
 */

#ifndef _SOLVE_DEFINE
#define _SOLVE_DEFINE

#include <stdlib.h>
#include "../../part2/src/hint.h"
#include "thread.h"

typedef struct StateQueue
{
  int start,end;
  int size;
  State * data;
  int capacity;
}StateQueue;
//State queue functions ( used for BFS)

/** Initializes the queue
 *  @param a pointer queue to the queue and the maximum size
 *  @return void
 */
void initQueue(StateQueue * queue,int capacity);

/** Inserts a State to the back of the queue
 *  @param a pointer queue to the queue and the state next
 *  @return void
 */
void insertQueue(StateQueue * queue,State next);

/** Removes the state in the front of the queue and returns it
 *  @param a pointer queue to the queue
 *  @return the first element in the queue
 */
State removeQueue(StateQueue * queue);

/** Deletes the queue, freeing it's memory
 *  @param a pointer queue
 *  @return void
 */
void deleteQueue(StateQueue * queue);


//solving functions
/** Solves the puzzle, using one or at least Nthreads threads.
 *  @param the suggested number of threads
 *  @return void
 */
Result solve(int Nthreads);

/** Solves the puzzle, using a BFS. When the queue has more than Nthread elements, each became a thread solving with DFS.
 *  @param the initial state and the suggested number of threads
 *  @return the result, if solved is equal to 1, the matrix corresponds to a solution, otherwise there is no solution for this initial state
 */
Result solveBFS(State start,int Nthreads);

/** Solves the puzzle, using a DFS ( backtracking).
 *  @param the initial state
 *  @return the result, if solved is equal to 1, the matrix corresponds to a solution, otherwise there is no solution for this initial state
 */
Result solveDFS(State *start);


//auxiliary functions
/** Returns the block to which the element in position r,c belongs
 *  @param the position (r,c) of the element
 *  @return the block
 */
int getBlockNumber(int r,int c);

/** Uses the information about rows,columns and blocks of the current state, calculates the available numbers
 *  @param the state
 *  @return the set, represent as a bitmask, of available numbers
 */
int getAvailableNumbers(const State * s);

/** Buids the next state, based on the current one and the select number
 *  @param state and the number
 *  @return the next state
 */
State buildNextState(State * cur,int num);

#endif
