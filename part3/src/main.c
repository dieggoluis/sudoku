/*
 *! @file main.c
 */

#include "thread.h"
#include "solve.h"

/* global matrix to represent sudoku puzzle */
int sudokuMatrix[9][9];

/* Main function */
int main (int argc, char**argv)
{
  int Nthreads,i,j;
  if (argc == 1)
    {
      //help
      printf("Este programa tem como entrada uma instância de sudoku(9x9) e encontra a solução dela caso ela exista.\nComo parametro é possível indicar o número de threads que serão utilizados Nthreads, 1 <= Ntrheads <= 100.\n");
	return 0;

     }

  //reads number of threads
  sscanf(argv[1], "%d", &Nthreads);
  if(Nthreads < 1 || Nthreads > MAX_THREADS)
    {
      printf("Número de threads inválido.");
      return 0;
    }
 
  //reads the sudoku matrix
  for (i = 0; i < 9; i++)
    {
      for(j = 0; j < 9; j++)
	{
	  char c; 
	  scanf(" %c", &c);
	  if(c == 'X')
	    sudokuMatrix[i][j] = -1;
	  else
	    sudokuMatrix[i][j] = c-'0';
	}
    }

  // checks result (solved or not)
  Result result = solve(Nthreads);
  if(result.solved != 0)
    {
      int l, m;
      for(l = 0; l < 9; l++)
	{
	  for(m = 0; m < 9; m++)
	    printf("%d ", result.sudokuMatrix[l][m]);
	  printf("\n");
	}
    }
  else
    {
      printf("Diagrama sem solução\n");
    }
  
  if (countThreads == 0)
    {
      printf("A fila nunca foi maior ou igual a %d, portanto um único thread foi utilizado\n", Nthreads);
    }
  else
    {
      printf("Foram utilizados %d threads.\n", countThreads);
    }
  
  return 0;
} /* main */


