#Resolução do diagrama

Este módulo encontra a solução para qualquer diagrama sudoku 9x9 caso ela exista.

##Requerimentos
***************
  - Compilador GCC com a biblioteca Pthread
  - Sistema Operacional UNIX

##Entrada
*********
O programa recebe como entrada uma matriz 9x9 que representa um diagrama sudoku. Neste diagrama, X representa um elemento que ainda não foi definido. Abaixo segue um exemplo de entrada:

          X 6 1 X X X X 9 1
          4 3 X X 9 5 X 6 X
          9 X X 1 X 2 X X 3
          X X X 4 X 1 X X 9
          5 X 9 X X X 7 X 1
          6 X X 2 X 9 X X X
          3 X X 9 X 8 X X 6
          X 8 X 7 3 X X 4 2
          X 9 X X X X 3 1 X

##Saída
*******
Caso a solução exista o programa a encontrará e a imprimirá na tela, como mostra o exemplo abaixo:

          8 6 1 3 4 7 2 9 5
          4 3 2 8 9 5 1 6 7
          9 5 7 1 6 2 4 8 3
          2 7 8 4 5 1 6 3 9 
          5 4 9 6 8 3 7 2 1 
          6 1 3 2 7 9 8 5 4
          3 2 4 9 1 8 5 7 6
          1 8 5 7 3 6 9 4 2
          7 9 6 5 2 4 3 1 8

Caso a solução não exista, o programa imprimirá na tela a seguinte mensagem:

          Diagrama sem solução

##Compilação
************

A compilação do módulo é feita executando o seguinte comando:

           make

##Execução
**********

A execução do módulo é feita executando o seguinte comando:

           ./main Ntrheads </caminho/para/o/arquivo/de/entrada/arquivo

No exemplo acima Ntrheads é o número sugerido de threads que serão usadas para resolver o problema, onde 1 <= Ntrheads <= 100.
